#!/usr/bin/env node

const lib = require('../tasks');
const path = require('path');

if (process.argv.length < 5) {
  return console.error('incomplete arguments');
}

let title = process.argv[2];
let ctrlFolder = path.resolve(process.argv[3]);
let outputFile = path.resolve(process.argv[4]);
let addQuota = process.argv[5] ? true : false;

lib.task2(title, ctrlFolder, outputFile, addQuota);
