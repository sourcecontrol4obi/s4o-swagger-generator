let head = `
swagger: "2.0"
info:
  version: "0.0.1"
  title: {{title}}
  schemes:
  # tip: remove http to make production-grade
  - http
  - https
# format of bodies a client can send (Content-Type)
consumes:
  - application/json
# format of the responses to the client (Accepts)
produces:
  - application/json
  - text/plain
x-a127-config: {}
x-a127-services: 
  defaultQuota:     
    provider: "volos-quota-memory"
    options:
      timeUnit: "minute"
      interval: 1
      allow: 40
  publicQuota:     
    provider: "volos-quota-memory"
    options:
      timeUnit: "minute"
      interval: 1
      allow: 20
  spikearrest:
    provider: "volos-spikearrest-memory"
    options:
      timeUnit: "second"
      bufferSize: 500
      allow: 1000
`;

let paths = `
  /{{controllerNamePlural}}:
    x-swagger-router-controller: {{controllerName}}
    x-a127-apply: 
      spikearrest: {}
      defaultQuota: {}
    parameters:
      - in: header
        name: authorization
        type: string
        required: true
    get:
      summary: get {{controllerNamePlural}}
      description: query {{controllerNamePlural}} that match query parameters
      operationId: get{{controllerNameCapitalizedPlural}}      
      tags: 
        - {{controllerNamePlural}}
      responses:
        200:
          description: OK
          schema: {}
        default:
          description: Default Response 
          schema:
            $ref: "#/definitions/DefaultResponse"
    post:
      summary: create {{controllerName}}
      description: creates new {{controllerName}}
      operationId: create{{controllerNameCapitalized}}
      tags: 
        - {{controllerNamePlural}}
      parameters:
        - in: body
          name: data
          schema:
            $ref: "#/definitions/New{{controllerNameCapitalized}}"
      responses:
        200:
          description: OK
          schema:
            $ref: "#/definitions/{{controllerNameCapitalized}}"
        default:
          description: Default Response 
          schema:
            $ref: "#/definitions/DefaultResponse"
  /{{controllerNamePlural}}/{id}:
    x-swagger-router-controller: {{controllerName}}
    x-a127-apply: 
      spikearrest: {}
      defaultQuota: {}
    parameters:
      - in: header
        name: authorization
        type: string
        required: true
    get:
      summary: get {{controllerName}}
      description: get a {{controllerName}} with id
      operationId: get{{controllerNameCapitalized}}
      tags: 
        - {{controllerNamePlural}}
      parameters:
        - in: path
          name: id
          type: string
          required: true
      responses:
        200:
          description: OK
          schema:
            $ref: "#/definitions/{{controllerNameCapitalized}}"
        default:
          description: Default Response 
          schema:
            $ref: "#/definitions/DefaultResponse"
    put:
      summary: update {{controllerName}}
      description: updates an existing {{controllerName}}
      operationId: update{{controllerNameCapitalized}}
      tags: 
        - {{controllerNamePlural}}
      parameters:
        - in: path
          name: id
          type: string
          required: true
        - in: body
          name: data
          schema:
            type: object
      responses:
        200:
          description: OK
          schema:
            $ref: "#/definitions/{{controllerNameCapitalized}}"
        default:
          description: Default Response 
          schema:
            $ref: "#/definitions/DefaultResponse"
`;

const { template, templateSettings, filter, map, toUpper } = require('lodash');
const fs = require('fs');
const pluralize = require('pluralize');

templateSettings.interpolate = /{{([\s\S]+?)}}/g;

/**
 * 
 * @param {String} string 
 */
let firstUpCase = (string) => {
  return toUpper(string[0]) + string.substr(1)
}

let task = (title, ctrlFolder, outputFile) => {
  let fileList = fs.readdirSync(ctrlFolder);
  fileList = filter(fileList, (file) => file.match(/\.js/));
  let fileNames = map(fileList, (file) => file.split('.')[0]);
  // console.log(fileNames);

  let headTemplate = template(head);
  let pathsTemplate = template(paths);

  let resultList = [];
  resultList.push(headTemplate({ title }));
  resultList.push('paths:')
  for (let file of fileNames) {
    resultList.push(pathsTemplate({
      controllerName: file,
      controllerNamePlural: pluralize.plural(file),
      controllerNameCapitalized: firstUpCase(file),
      controllerNameCapitalizedPlural: firstUpCase(pluralize.plural(file))
    }));
  }

  resultList.push('definitions:');

  for (let file of fileNames) {
    resultList.push(
      `
  New${firstUpCase(file)}:
    type: object
  ${firstUpCase(file)}:
    type: object`);
  }

  resultList.push(`\n  DefaultResponse: {}`);
  // console.log(resultList.join(''))

  fs.writeFileSync(outputFile, resultList.join(''));
}

module.exports = {
  task
}
