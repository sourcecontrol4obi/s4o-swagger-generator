const { template,
  templateSettings,
  filter,
  map, toUpper, first,
  last, camelCase, keys,
  pickBy, forEach,
  flatten, values, lowerCase,
  escape, toLower
} = require('lodash');
const fs = require('fs');
const urlParse = require('url-parse');

templateSettings.interpolate = /{{([\s\S]+?)}}/g;

let head =
  `swagger: "2.0"
info:
  version: "0.0.1"
  title: {{title}}
schemes:
# tip: remove http to make production-grade
- http
- https
# format of bodies a client can send (Content-Type)
consumes:
  - application/json
  - multipart/form-data
  - text/plain
# format of the responses to the client (Accepts)
produces:
  - application/json
  - text/plain
  - text/csv
  - text/html
  - image/png
  - image/gif
  - image/jpeg
host: {{host}}
basePath: {{basePath}}
x-a127-config: {}
x-a127-services: 
  defaultQuota:     
    provider: "volos-quota-memory"
    options:
      timeUnit: "minute"
      interval: 1
      allow: 40
`;

let pathHeader = `
  {{path}}:
    x-swagger-router-controller: {{controllerName}}
    x-a127-apply:       
      defaultQuota: {}`;

let pathHeaderNoQuota = `
  {{path}}:
    x-swagger-router-controller: {{controllerName}}`;

let pathDefinition = `
    {{method}}:
      summary: {{summary}}
      description: {{description}}
      operationId: {{operationId}}
      tags: 
        - {{tag}}`;

let parametersHeader = `
      parameters:`;

let parameterBasic = `
        - in: {{location}}
          name: {{name}}
          description: {{description}}
          required: {{required}}`;

let parameterBody = `
        - in: body
          name: data
          schema:
            $ref: "#/definitions/{{modelName}}"`;

let responseDefinition = `
      responses:
        default:
          description: Default Response 
          schema:
            $ref: "#/definitions/DefaultResponse"`;

let successResponseDefinition = `
        200:
          description: OK
          schema:
            $ref: "#/definitions/{{modelName}}"`;

// templates
let docHeadTemplate = template(head);
let pathHeaderTemplate = template(pathHeader);
let pathHeaderNoQuotaTemplate = template(pathHeaderNoQuota);
let pathDefinitionTemplate = template(pathDefinition);
let parametersHeaderTemplate = template(parametersHeader);
let parameterBasicTemplate = template(parameterBasic);
let parameterBodyTemplate = template(parameterBody);
let responseDefinitionTemplate = template(responseDefinition);
let successResponseDefinitionTemplate = template(successResponseDefinition);

// regular expressions
let regexStart = /\/\*{2}/;
let regexEnd = /\*\//;
let definitionRegex = /\@apiDefine\s{1,}(\w{1,})/;
let routeRegex = /\@api\s\{(\w{1,})\}\s{1,}([\w\/\:\-]{1,})\s{1,}([\w\s]{1,}\w)/;
let paramRegex = /\@apiParam\s(\(\w{1,}\)\s)?\{([^\s]{1,})\}\s\[?([\w\.]{1,}(?:\[\])?)(?:[\d\.\{\}]{1,})?\=?\w{0,}\]?\s?([^\n\*]{0,})/;
let headerRegex = /\@apiHeader\s(\(\w{1,}\)\s)?\{([^\s]{1,})\}\s([\w\[\]]{1,})\=?\w{0,}\s?([^\n\*]{0,})/;
let typeRegex = /\{(\w{1,})((?:\[\])?)(?:\{([^}]{1,})\})?\=?(?:([^}]{1,}))?\}/;
let apiNameRegex = /\@apiName\s(\w*)/;
let apiGroupRegex = /\@apiGroup\s(\w*)/;
let apiUseRegex = /\@apiUse\s(\w*)/;

/**
 * 
 * @param {String} string 
 */
let firstUpCase = (string) => {
  return toUpper(string[0]) + string.substr(1)
}

/**
 * @param {String} data 
 */
let getRange = (data) => {
  if (data.match(/\d{0,}\.{2}\d{0,}/)) {
    let parts = data.split('..');
    let result = {};
    if (Number(parts[0]) > 0) result['minLength'] = Number(parts[0]);
    if (Number(parts[1]) > 0) result['maxLength'] = Number(parts[1]);
    return result;
  }
  if (data.match(/\d{0,}\-\d{0,}/)) {
    let parts = data.split('-');
    let result = {};
    if (Number(parts[0]) > 0) result['min'] = Number(parts[0]);
    if (Number(parts[1]) > 0) result['max'] = Number(parts[1]);
    return result;
  }
}

let getType = (data) => {
  let parts = typeRegex.exec(`{${data}}`);
  if (!parts[2] && !parts[3]) return firstUpCase(parts[1]);
  if (parts[2] && parts[2] === '[]') {
    return {
      type: 'Array',
      items: firstUpCase(parts[1])
    }
  }
  let result = {
    type: firstUpCase(parts[1])
  }
  if (parts[3]) {
    result['range'] = getRange(parts[2]);
  }
  if (parts[4]) {
    result['enum'] = parts[3].replace(/[\'\"]/g, '').split(',');
  }
  return result;
}

let getParams = (arr) => {
  let params = {};
  arr.forEach((line) => {
    let regex = paramRegex.test(line) ? paramRegex : headerRegex.test(line) ? headerRegex : undefined;
    if (regex && line.indexOf('.') < 0) {
      let parts = regex.exec(line);
      let name = parts[3];
      let optional = false;
      if (name.match(/\[\w{1,}\]/)) {
        name = name.replace(/\]|\[/g, '');
        optional = true;
      }
      if (new RegExp(`\\[\\s{0,}${name}`).test(line)) {
        optional = true;
      }
      params[name] = { optional, type: getType(parts[2]), description: parts[4], header: regex === headerRegex };
    }
  });
  return params;
}

let processDefinitions = (arr, definitions) => {
  let start = first(arr);
  let params = {};
  if (definitionRegex.test(start)) {
    let name = definitionRegex.exec(start)[1];
    params = getParams(arr.slice(1));
    definitions[name] = params;
    return true;
  }
  return false;
}

let getRouteName = (arr) => {
  for (let line of arr) {
    if (apiNameRegex.test(line)) {
      return apiNameRegex.exec(line)[1];
    }
  }
  return '';
}

let getRouteGroup = (arr) => {
  for (let line of arr) {
    if (apiGroupRegex.test(line)) {
      return apiGroupRegex.exec(line)[1];
    }
  }
  return '';
}

let getRouteParamList = (path) => {
  let routeParamRegex = /\:(\w{1,})/g;
  let match = path.match(routeParamRegex);
  match = match ? match : [];
  return match.map((v) => v.replace(':', ''));
}

let resolveUses = (arr, definitions) => {
  let params = {};
  for (let line of arr) {
    if (apiUseRegex.test(line)) {
      let name = apiUseRegex.exec(line)[1];
      params = { ...params, ...definitions[name] };
    }
  }
  return params;
}

let processRoutes = (arr, routes, definitions) => {
  let start = first(arr);
  let params = {};
  if (routeRegex.test(start)) {
    let parts = routeRegex.exec(start);
    let method = parts[1];
    let path = parts[2];
    let description = parts[3];
    let name = getRouteName(arr.slice(1));
    let group = getRouteGroup(arr.slice(1));
    let params1 = resolveUses(arr.slice(1), definitions);
    let params2 = getParams(arr.slice(1));
    params = { ...params1, ...params2 };
    routes[`${toLower(method)};${path}`] = {
      controller: last(arr),
      method: camelCase(name),
      tag: group,
      description,
      routeParams: getRouteParamList(path),
      params
    }
    return true;
  }
  return false;
}

let generateDocType = (type, spacing) => {
  let result = [];
  if (typeof type === 'string') result.push(`type: ${lowerCase(type)}`);
  else {
    if (lowerCase(type.type) === 'any') {
      if (type.description) {
        result.push(JSON.stringify({
          description: `${escape(type.description)}`
        }));
      } else {
        result.push(JSON.stringify({}));
      }
    } else {
      result.push(`type: ${lowerCase(type.type)}`);
      if (type.enum) {
        result.push(`enum: ${JSON.stringify(type.enum)}`);
      }
      if (type.items) {
        result.push('items:');
        result.push(`  type: ${lowerCase(type.items)}`);
      }
      if (type.range) {
        forEach(type.range, (v, k) => {
          result.push(`${k}: ${v}`);
        });
      }
      if (type.description) {
        result.push(`description: '${escape(type.description)}'`);
      }
    }
  }
  let space = new Array(spacing).fill(' ').join('');
  return `${space}${result.join('\n' + space)}`;
}

let generateDocDefinitions = (models) => {
  let result = ['definitions:'];
  forEach(models, (modelProps, modelName) => {
    result.push(`  ${modelName}:`);
    result.push(`    type: object`);
    let required = keys(pickBy(modelProps, (value) => !value.optional));
    if (required.length > 0) {
      result.push(`    required:`);
      forEach(required, (name) => {
        result.push(`      - ${name}`);
      });
    }
    result.push(`    properties:`);
    forEach(modelProps, (data, name) => {
      result.push(`      ${name}:`);
      result.push(generateDocType(typeof data.type === 'string' ? data : { ...data.type, description: data.description }, 8));
    });
    if (last(result).match('properties:')) result.pop();
  });
  return `\n${result.join('\n')}`;
}

let createDoc = (path, method, data, pathMap, models, addQuota) => {
  const pathHeaderTmpl = addQuota ? pathHeaderTemplate : pathHeaderNoQuotaTemplate;
  if (!pathMap[path]) pathMap[path] = [pathHeaderTmpl({ path: path.replace(/\:(\w{1,})/g, '{$1}'), controllerName: data.controller })];
  pathMap[path].push(pathDefinitionTemplate({ method, summary: data.description, description: data.description, operationId: data.method, tag: data.tag }));
  let responseModel = undefined;
  let params = keys(data.params);
  if (params.length > 0) {
    pathMap[path].push(parametersHeaderTemplate({}));
    let headerParams = pickBy(data.params, (value) => value.header);
    forEach(headerParams, (param, key) => {
      pathMap[path].push(parameterBasicTemplate({ location: 'header', name: key, description: `'${escape(param.description)}'`, required: !param.optional }));
      pathMap[path].push('\n' + generateDocType(param.type, 10));
    });
    let pathParams = pickBy(data.params, (value, key) => data.routeParams.indexOf(key) >= 0);
    forEach(pathParams, (param, key) => {
      pathMap[path].push(parameterBasicTemplate({ location: 'path', name: key, description: `'${escape(param.description)}'`, required: true }));
      pathMap[path].push('\n' + generateDocType(param.type, 10));
    });
    let otherParams = pickBy(data.params, (value, key) => keys(headerParams).indexOf(key) < 0 && keys(pathParams).indexOf(key) < 0);
    switch (method) {
      case 'get':
        forEach(otherParams, (param, key) => {
          if ((/object/i).test(param.type) ||
            (/object/i).test(param.type.type) ||
            (/any/i).test(param.type) ||
            (/any/i).test(param.type.type)) return;
          pathMap[path].push(parameterBasicTemplate({ location: 'query', name: key, description: `'${escape(param.description)}'`, required: !param.optional }));
          pathMap[path].push('\n' + generateDocType(param.type, 10));
        });
        break;
      case 'post':
        models[firstUpCase(data.method)] = otherParams;
        pathMap[path].push(parameterBodyTemplate({ modelName: firstUpCase(data.method) }));
        // responseModel = firstUpCase(data.method);
        break;
      case 'put':
      case 'patch':
        models[firstUpCase(data.method)] = otherParams;
        pathMap[path].push(parameterBodyTemplate({ modelName: firstUpCase(data.method) }));
        // responseModel = firstUpCase(data.method);
        break;
    }
    if (last(pathMap[path]).match('parameters:')) pathMap[path].pop();
  }
  pathMap[path].push(responseDefinitionTemplate({}));
  if (responseModel) pathMap[path].push(successResponseDefinitionTemplate({ modelName: responseModel }));
}

let getCommandLineArg = (arg) => {
  let parts = arg.match(/\$\{(\w{1,})\}/);
  return process.env[parts[1]];
}

let task = (title, ctrlFolder, outputFile, addQuota) => {
  let fileList = fs.readdirSync(ctrlFolder);
  fileList = filter(fileList, (file) => file.match(/\.js$/) || file === 'apidoc.json');
  let fileNames = map(fileList, (file) => file !== 'apidoc.json' ? file.split('.')[0] : file);

  let definitions = {};
  let routes = {};
  let comments = [];
  let config = { title, host: 'localhost', basePath: '/' };

  for (let file of fileNames) {
    if (file === 'apidoc.json') {
      let content = fs.readFileSync(`${ctrlFolder}/${file}`);
      content = content.toString();
      let obj = JSON.parse(content);
      if (obj.name) config.title = obj.name;
      if (obj.url) {
        let url = obj.url;
        if (url.startsWith('$')) url = getCommandLineArg(url);
        config.host = urlParse(url, true).host;
      }
      if (obj.sampleUrl) {
        let url = obj.sampleUrl
        if (url.startsWith('$')) url = getCommandLineArg(url);
        let path = urlParse(url, true).pathname;
        config.basePath = path ? path : config.basePath;
      }
      continue;
    }

    let content = fs.readFileSync(`${ctrlFolder}/${file}.js`);
    content = content.toString();

    let lines = content.split('\n');
    let currentComment = [];
    let started = false;

    for (let line of lines) {
      if (regexStart.test(line)) {
        currentComment.length = 0;
        started = true;
        continue;
      }
      if (regexEnd.test(line)) {
        started = false;
        currentComment.push(file);
        if (!processDefinitions([...currentComment], definitions)) comments.push([...currentComment]);
        continue;
      }
      if (started) {
        currentComment.push(line);
      }
    }
  }

  // console.dir(definitions, { depth: 4 });

  for (let comment of comments) {
    processRoutes(comment, routes, definitions);
  }

  console.dir(routes, { depth: 6 });

  let pathMap = {};
  let models = {};
  let paths = keys(routes);
  paths.forEach((path) => {
    let parts = path.split(';');
    createDoc(parts[1], parts[0], routes[path], pathMap, models, addQuota);
  });

  // console.log(models);
  let docDefs = generateDocDefinitions(models);

  fs.writeFileSync(outputFile,
    flatten([docHeadTemplate(config), 'paths:']
      .concat(values(pathMap), docDefs, `\n  DefaultResponse: {}`)
    ).join('')
  );
}

module.exports = {
  task
}
