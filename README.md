### installation
```
npm i -g git+https://sourcecontrol4obi@bitbucket.org/sourcecontrol4obi/s4o-swagger-generator.git
```

### usage
```shell
s4o-swagger-generator [project-name] [controllers-folder-path] [output-file-path]
```

use [apidocjs](http://apidocjs.com) for controller documentation, and automatically have your docs converted to swagger

### disclaimer
this tool is still under development, user's descretion is adviced
